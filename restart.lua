mp.register_script_message('restart', function()
	local files = {}
	for _,val in ipairs(mp.get_property_native('playlist')) do
		files[#files+1] = val.filename
	end

	mp.command('write-watch-later-config')
	mp.commandv('run', 'mpv', unpack(files))
	mp.command('quit')
end)

