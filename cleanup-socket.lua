mp.register_event("shutdown", function()
	local socket_file = mp.get_property("input-ipc-server")
	if socket_file then
		os.remove(socket_file)
	end
end)

