if not os.getenv("DISPLAY") then return end

mp.enable_messages('error')

function notify(u, t, b)
	mp.command_native({'run', 'notify-send', '-a mpv', '-u '..u, t, b})
end

function isURL(t)
	return t and t:find('^%a+://') == 1
end

mp.register_event('log-message', function(e)
	if not mp.get_property_native('focused') and e.prefix == 'ytdl_hook' then
		data = mp.get_property_native('path')
		notify('critical', e.prefix, ('%s\n%s'):format(e.text, data or ''))
	end
end)

mp.register_event('start-file', function(e)
if mp.get_property_native('focused') then return end
	data = mp.get_property_native('path')
	if isURL(data) then
		notify('low', '[MPV] Loading...', data)
	end
end)

mp.register_event('file-loaded', function(e)
if mp.get_property_native('focused') then return end
	data = mp.get_property_native('path')
	title = mp.get_property_native('media-title')
	if isURL(data) then
		notify('low', '[MPV] Loaded', ('%s\n%s'):format(title, data))
	end
end)

