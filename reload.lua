mp.register_script_message('reload', function()
	local playlist_count = mp.get_property_number("playlist-count")
	local playlist_pos = mp.get_property_number("playlist-pos")
	local time_pos = mp.get_property_number("time-pos")
	local playlist = {}

	for i = 0, playlist_count - 1 do
		playlist[i] = mp.get_property("playlist/"..i.."/filename")
	end

	for i = 0, playlist_count - 1 do
		mp.commandv("loadfile", playlist[i],
			i == 0 and "replace" or "append",
			0,
			i == playlist_pos and "start="..time_pos or '')
	end

	mp.set_property_number("playlist-pos", playlist_pos)
end)

